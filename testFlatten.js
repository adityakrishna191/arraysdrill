const { nestedArray } = require("./arrays");
const flatten = require("./flatten");

// const result = flatten(nestedArray, 2);
// console.log(result);

// const result = flatten([0,[1],[[2]],[[[3]]]],2);
// console.log(result);

const result = flatten([0,[1],[[[2]]]],1);
console.log(result);