const each = require("./each.js");

function map (elements, cb) {
    // How map works: Map calls a provided callback function once for each element in an array, in order, and constructs a new array from the result.
    
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // each function can act as the iterator here
    
    // Return the new array.

    return each(elements, cb);
    
}

module.exports = map;
