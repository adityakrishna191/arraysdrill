const each = require("./each");


function find (elements, cb) {
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.

    if(!Array.isArray(elements) || cb === undefined) {
        return [];
    }

    const result = each(elements, cb);
    // console.log(result);
    for(let i = 0; i < result.length; i++) {
        if(result[i]!== undefined){
            return result[i];
        }
    }
    return undefined;

}

module.exports = find;