
function each(elements, cb) {
    
    // This only needs to work with arrays.
    
    if(elements === undefined || !Array.isArray(elements) || elements.length === 0) {
        return [];
    }

    if(cb === undefined ) {
        return [];
    }

    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // You should also pass the index into `cb` as the second argument
    
    const result = [];

    for(let i = 0; i < elements.length; i++) {
        // return cb(elements[i], i, elements);
        
        result.push(cb(elements[i], i, elements));
        
    }

    return result;


}

module.exports = each;