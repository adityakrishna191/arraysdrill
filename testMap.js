const mapFunction = require("./map.js");

const sampleArrays = require("./arrays.js");
const simpleArray = sampleArrays.items; //[1, 2, 3, 4, 5, 5]

const callBackFunction = function (item, index, elements) {
    return item*2;
}

const result = mapFunction(simpleArray, callBackFunction);
console.log(result);