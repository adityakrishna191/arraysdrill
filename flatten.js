function flatten(elements, depth = 1) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    // [0,[1],[[2]],[[[3]]]]

    const result = [];
    function flattenHelp(elements){ // helper function for recursion
        for (let i = 0; i < elements.length; i++){
            let element = elements[i];
            if(element===undefined){
                continue;
            }else{
                if(Array.isArray(element)){
                    flattenHelp(element);
                }else{    
                    result.push(element);
                }
            }            
        }
    }
    flattenHelp(elements);
    // console.log(elements);    
    // console.log(result);
    return result;
}

module.exports = flatten;