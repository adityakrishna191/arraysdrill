const each = require("./each");

function filter(elements, cb) {
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test

    if(!Array.isArray(elements) || cb === undefined){
        return [];
    }

    const result = [];
    
    for(let i=0; i<elements.length; i++){
        if(cb(elements[i])){
            result.push(elements[i]);
        }
    }
    
    return result;
}

module.exports = filter;