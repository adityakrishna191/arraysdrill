const reduceFunction = require("./reduce.js");

const sampleArrays = require("./arrays.js");
const simpleArray = sampleArrays.items; //[1, 2, 3, 4, 5, 5]

const callBackFunction = function (startingValue, item) {
    return startingValue + item;
}

const result = reduceFunction(simpleArray, callBackFunction, 10);
console.log(result);