const eachFunction = require("./each.js"); 

const sampleArrays = require("./arrays.js"); //console.log(sampleArray);
const simpleArray = sampleArrays.items; //[1, 2, 3, 4, 5, 5]

const callBackFunction = function (item, index, elements) {
    // console.log(item, index);
    
    // return item; 

    return item + 2;
}

// console.log(eachFunction, sampleArray); //to test imports value

const result = eachFunction(simpleArray, callBackFunction);

console.log(result);